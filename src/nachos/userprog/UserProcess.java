package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;

import java.io.EOFException;
import java.util.LinkedList;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 *
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 *
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
/**
 * @author 豫
 *
 */
/**
 * @author 豫
 *
 */
public class UserProcess {
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
		pid = numOfProcess;	//	进程数从1开始，进程号从0开始
		numOfProcess++;
		openfile[0] = UserKernel.console.openForReading();
		openfile[1] = UserKernel.console.openForWriting();
		int numPhysPages = Machine.processor().getNumPhysPages();
		pageTable = new TranslationEntry[numPhysPages];
		for (int i = 0; i < numPhysPages; i++)
			pageTable[i] = new TranslationEntry(i, 0, false, false, false, false);
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key <tt>Kernel.processClassName</tt>.
	 *
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to load
	 * the program, and then forks a thread to run it.
	 *
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
		if (!load(name, args))
			return false;

		thread =(UThread) new UThread(this).setName(name);
		thread.fork();

		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch. Called by
	 * <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 * 保存状态， 将处理器的页表保存为当前页表
	 */
	public void restoreState() {
		Machine.processor().setPageTable(pageTable);
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for the
	 * null terminator, and convert it to a <tt>java.lang.String</tt>, without
	 * including the null terminator. If no null terminator is found, returns
	 * <tt>null</tt>.
	 * 
	 * 在虚拟内存中读取一个null结尾的字符串， 最多读maxLength+1个字节。如果找不到这样的字符串，返回null
	 *
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including the
	 *            null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)	//	哪里遇到了null了，说明读取结束了，把前面的返回出来。
				return new String(bytes, 0, length);
		}

		return null;	//	如果一直没遇到null，说明不存在。
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 *
	 *从虚拟内存的给定地址中读取给定长度的数据（数组的长度）存入数组中（data）
	 *
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array. This
	 * method handles address translation details. This method must <i>not</i>
	 * destroy the current process if an error occurs, but instead should return the
	 * number of bytes successfully copied (or zero if no data could be copied).
	 *
	 *把虚拟内存中的数据读到data数组中。虚拟内存中的值需要转换成物理地址去找
	 *
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 *            注意这个是具体的虚拟地址，而不是页号。
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 *            从数组的第几个元素开始写。
	 *            
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0 && offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();
		if(length<0) {
			return -1;
		}
		
		//	如果需要读的长度比当前虚拟地址到页表最后的所有长度都还大，那肯定没有那么多数据供你读
		if(length>(pageSize*numPages-vaddr)) {
			length = pageSize*numPages-vaddr;
		}
		//	如果你要存数据的数组（data）不如你要存的数据那么大，那么肯定也存不下
		if(length>data.length-offset) {
			length = data.length- offset;
		}
		int cur = 0;	//	已转换的字符数
		
		do {
			//找到在哪一页
			int pageNum = Processor.pageFromAddress(vaddr+cur);	//	获得页号
			if(pageNum<0||pageNum>=pageTable.length) {
				//	页号不合法
				return 0;
			}
			//找到页偏移
			int pageOffset = Processor.offsetFromAddress(vaddr+cur);
			int leftByte = pageSize-pageOffset;	//	内部碎片，一个页中没用完的部分
			//	比较页剩下的部分和文件剩下的部分,一般来说都是页剩下的部分少，这里就是计算这一次要读的部分
			//  为了每次整页整页的取，一般的也就第一次和最后一次比较特殊。中间的都是整的
			//  也就是说中间的页pageOffset都是0，所以amount也就等于页大小。最后的时候可能文件
			//	剩余的长度不足一页了，那么就只需要读文件剩余的长度大小就可以了。
			int amount = Math.min(leftByte, length-cur);
			
			//	读的地址。先找到这个虚拟页对应的物理页号，物理页号*页大小得到那个物理页的位置，然后加上偏移量
			int realAddr = pageTable[pageNum].ppn*pageSize+pageOffset;

			//	进行数组copy
			System.arraycopy(memory, realAddr, data, offset+cur, amount);
			cur = cur+amount;
			
		} while(cur<length);
		return cur;
		
		

		// for now, just assume that virtual addresses equal physical addresses
		//只考虑了虚拟地址等于物理地址的情况。
//		if (vaddr < 0 || vaddr >= memory.length)
//			return 0;
//
//		// 如果需要的长度比总长度-首地址大，说明没有那么多的数据可供读了，那么要修改length。
//		int amount = Math.min(length, memory.length - vaddr);
//		System.arraycopy(memory, vaddr, data, offset, amount);
//
//		return amount;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual memory.
	 * Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 *
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory. This
	 * method handles address translation details. This method must <i>not</i>
	 * destroy the current process if an error occurs, but instead should return the
	 * number of bytes successfully copied (or zero if no data could be copied).
	 *
	 *把数据从特定数组存入虚拟内存
	 *
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0 && offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();
		if(length<0) {
			return -1;
		}
		
		if(length>(pageSize*numPages-vaddr)) {
			length = pageSize*numPages-vaddr;
		}
		if(length>data.length-offset) {
			length = data.length-offset;
		}
		
		int cur = 0;
		//同上
		
		do {
			int pageNum = Processor.pageFromAddress(vaddr+cur);
			if(pageNum<0||pageNum>=pageTable.length) {
				return 0;
			}
			int pageOffset = Processor.offsetFromAddress(vaddr+cur);
			int leftByte = pageSize-pageOffset;
			int amount = Math.min(leftByte, length-cur);
			int realAddr = pageTable[pageNum].ppn*pageSize+pageOffset;
			System.arraycopy(data, offset+cur, memory, realAddr, amount);
			cur=cur+amount;
		}while (cur<length);

		return cur;
//		// for now, just assume that virtual addresses equal physical addresses
//		if (vaddr < 0 || vaddr >= memory.length)
//			return 0;
//
//		int amount = Math.min(length, memory.length - vaddr);
//		System.arraycopy(data, offset, memory, vaddr, amount);
//
//		return amount;
	}

	/**
	 * Load the executable with the specified name into this process, and prepare to
	 * pass it the specified arguments. Opens the executable, reads its header
	 * information, and copies sections and arguments into this process's virtual
	 * memory.
	 *
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			numPages += section.getLength();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		numPages += stackPages;
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		numPages++;

		if (!loadSections())
			return false;

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib.assertTrue(writeVirtualMemory(entryOffset, stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib.assertTrue(writeVirtualMemory(stringOffset, new byte[] { 0 }) == 1);
			stringOffset += 1;
		}

		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into memory.
	 * If this returns successfully, the process will definitely be run (this is the
	 * last step in process initialization that can fail).
	 * 给当前进程分配内存，把COFF模块加载到内存中
	 *
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
		//请求锁
		UserKernel.allocateMemoryLock.acquire();
		
//		if (numPages > Machine.processor().getNumPhysPages()) {
//			coff.close();
//			Lib.debug(dbgProcess, "\tinsufficient physical memory");
//			return false;
//		}
		
		if(numPages>UserKernel.memoryLinkedList.size()) {	//	和空闲页表中页的个数进行比较
			//不能装载
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			UserKernel.allocateMemoryLock.release();
			return false;
			
		}
		
		pageTable = new TranslationEntry[numPages];	//	创建和初始化页表，共有numPages页
		for(int i=0;i<numPages;i++) {
			int nextPage=UserKernel.memoryLinkedList.remove();//内存中找一个空闲页出队
			pageTable[i]=new TranslationEntry(i, nextPage, true, false, false, false);
		}
		UserKernel.allocateMemoryLock.release();

		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);	//	获得section

			Lib.debug(dbgProcess,"\tinitializing " + section.getName() + 
					" section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;	//	在这个section里找所有的虚拟页
				pageTable[vpn].readOnly = section.isReadOnly();
				//所以需要把这个虚拟地址在页表中对应的物理地址作为地址传到下面的方法中
				section.loadPage(i, pageTable[vpn].ppn);

				
				// for now, just assume virtual addresses=physical addresses
//				section.loadPage(i, vpn);
			}
		}

		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {

		// 将页表的页放入空闲页列表中
		releaseResource();
		for (int i = 0; i < 16; i++) {
			if (openfile[i] != null) {
				openfile[i].close();
				openfile[i] = null;// 页表项置为null
			}
			// 将该用户进程占用的内存加入空闲内存链表中
		}
		coff.close();

	}
	
	protected void releaseResource() {
		for (int i = 0; i < pageTable.length; ++i)
			if (pageTable[i].valid) {
				UserKernel.deletePage(pageTable[i].ppn);
				pageTable[i] = new TranslationEntry(pageTable[i].vpn, 0, false,
						false, false, false);
			}
		numPages = 0;
	}
	
	
//	protected void unloadSections() {
//		
//	}

	/**
	 * Initialize the processor's registers in preparation for running the program
	 * loaded into this process. Set the PC register to point at the start function,
	 * set the stack pointer register to point at the top of the stack, set the A0
	 * and A1 registers to argc and argv, respectively, and initialize all other
	 * registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

	/**
	 * Handle the halt() system call.
	 */
	private int handleHalt() {
		//	只有主进程才可以停机
		if(pid==0) {
			Machine.halt();
			
		}


		Lib.assertNotReached("Machine.halt() did not halt machine!");
		return 0;
	}
	
	
	private int handleCreate(int fileAddr) {
		String fileName = readVirtualMemoryString(fileAddr, 256);	//文件名不能超过256个字符
		if(fileName == null) {
			return -1;	//	说明该文件名不存在
		}
		int position = getPosition();	//	找到存储位置
		if(position == -1) {
			return -1;
		} else {
			openfile[position] = ThreadedKernel.fileSystem.open(fileName, true);	//	新建文件
			return position;
		}
		
	}
	
	private int handleOpen(int fileAddr) {
		String fileName = readVirtualMemoryString(fileAddr, 256);	//文件名不能超过256个字符
		if(fileName == null) {
			return -1;	//	说明该文件名不存在
		}
		int position = getPosition();
		if(position == -1) {
			return -1;
		} else {
			openfile[position] = ThreadedKernel.fileSystem.open(fileName, false);	//	打开文件
			return position;
		}
	}
	
	
	/**
	 * 从文件中读取数据，存入内存中
	 * @param position	文件在打开表中的位置
	 * @param bufferAddr	要存入的内存地址
	 * @param length	文件长度
	 * @return
	 */
	private int handleRead(int position, int bufferAddr, int length) {
		//不存在这个文件的情况
		if(position>15 || position <0 ||openfile[position]==null) {
			return -1;
		} 
		
		byte temp[] = new byte[length];	//	要读取文件的长度
		int readNumber = openfile[position].read(temp, 0, length);	//	利用openfile的read方法
		if(readNumber <= 0) {
			//	读取失败
			return 0;
		} 
		int writeNumber = writeVirtualMemory(bufferAddr, temp);	//	向缓冲区中写信息，返回写入的数量
		return writeNumber;	
	}
	
	
	/**
	 * 从内存中读取数据写入文件中
	 * @param position	文件的位置
	 * @param bufferAddr	内存地址
	 * @param length	数据长度
	 * @return
	 */
	private int handleWrite(int position, int bufferAddr, int length) {
		if(position>15 || position <0 ||openfile[position]==null) {
			return -1;
		} 
		byte temp[] = new byte[length];
		int readNumber = readVirtualMemory(bufferAddr, temp);	//	从内存中读数据
		if(readNumber <=0) {
			return 0;
		}
		int writeNumber = openfile[position].write(temp, 0, length);
		if(writeNumber<length) {
			return -1;	//	应该写的内容是length， 但是事实上没有写那么多
		}
		return writeNumber;
		
	}
	
	private int handleClose(int position) {	//	关闭打开的文件，也就是从打开表中删除
		if(position>15 || position <0 ||openfile[position]==null) {
			return -1;
		} 
		openfile[position].close();
		openfile[position]=null;	//	把数组中的这个对象释放
		return 0;
	}
	
	//从文件系统中移出这个文件
	private int handleUnlink(int fileAddr) {
		String fileName = readVirtualMemoryString(fileAddr, 256);
		if(fileName == null) {
			return 0;	//	根本没有这个文件，不用删
		} 
		if(ThreadedKernel.fileSystem.remove(fileName)) {
			//	删除成功
			return 0;
		} else {
			return -1;
		}
	}
	
	
	//处理exec系统调用，创建一个子进程并执行,返回它的pid
	
	
	/**
	 * @param fileAddr 	文件地址
	 * @param argc		参数count
	 * @param argvAddr	参数表地址
	 * @return			子进程的pid
	 */
	private int handleExec(int fileAddr, int argc, int argvAddr) {
		//根据文件地址找到文件名
		String fileName = readVirtualMemoryString(fileAddr, 256);
		
		if(fileName ==null||argc<0||argvAddr>numPages*pageSize) {
			return -1;
		}
		
		//参数表
		String args[] = new String[argc];	//	来自命令行的参数表
		for(int i=0;i<argc;i++) {
			byte[] argsAddr = new byte[4];
			
			//根据首地址找每个参数的地址，但要判断参数表有没有越界
			if(readVirtualMemory(argvAddr+i*4, argsAddr)>0) {
				args[i]=readVirtualMemoryString(Lib.bytesToInt(argsAddr, 0), 256);
			}
		}
		UserProcess process = UserProcess.newUserProcess();	//	创建一个新的用户进程
		if(!process.execute(fileName, args)) {
			return -1;
		}
		process.parentProcess = this;
		this.childProcess.add(process);
		return process.pid;
	}
	
	//关闭进程
	private int handleExit(int status) {
		coff.close();
		for(int i=0;i<16;i++) {
			if(openfile[i]!=null) {
				openfile[i].close();
				openfile[i]=null;
			}
		}
		this.status = status;
		normalExit = true;
		
		if(parentProcess!=null) {
			joinLock.acquire();
			joinCondition.wake();	//	唤醒被join的父进程
			joinLock.release();
			parentProcess.childProcess.remove(this);	//	在父进程中删除
		}
		unloadSections();
		KThread.finish();	//	当前进程完成
		
		//最后一个进程exit的话，系统就停机了
		if(numOfRunningProcess==1) {
			Machine.halt();
		}
		numOfRunningProcess--;
		return 0;
	}
	
	private int handleJoin(int pid, int statusAddr) {
		UserProcess process = null;
		
		//必须这个要join的进程pid是当前进程的子进程，才可以进行join。
		//遍历子进程数组进行搜索
		for(int i=0;i<childProcess.size();i++) {
			if(pid==childProcess.get(i).pid) {
				process = childProcess.get(i);
				break;
			}
		}
		if(process==null||process.thread==null) {
			return -1;
		}
		
		//获得子进程的锁
		process.joinLock.acquire();
//		process.thread.join();
		process.joinCondition.sleep();
		process.joinLock.release();
		
		byte[] childStatus = new byte[4];
		Lib.bytesFromInt(childStatus, 0, process.status);
		int writeByte = writeVirtualMemory(statusAddr, childStatus);
		if(process.normalExit&&writeByte==4) {
			return 1;
		}
		return 0;
	}
	
	private int getPosition() {
		for(int i=0;i<16;i++) {
			if(openfile[i]==null) {
				return i;
			}
		}
		return -1;
	}

	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2, syscallJoin = 3, syscallCreate = 4,
			syscallOpen = 5, syscallRead = 6, syscallWrite = 7, syscallClose = 8, syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 *
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
	 * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
	 *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
	 *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		switch (syscall) {
		case syscallHalt:
			return handleHalt();
		case syscallExit:
			return handleExit(a0);
		case syscallExec:
			return handleExec(a0, a1, a2);
		case syscallJoin:
			return handleJoin(a0, a1);
		case syscallCreate:
			return handleCreate(a0);
		case syscallOpen:
			return handleOpen(a0);
		case syscallRead:
			return handleRead(a0, a1, a2);
		case syscallWrite:
			return handleWrite(a0, a1, a2);
		case syscallClose:
			return handleClose(a0);
		case syscallUnlink:
			return handleUnlink(a0);

		default:
			Lib.debug(dbgProcess, "Unknown syscall " + syscall);
			Lib.assertNotReached("Unknown system call!");
		}
		return 0;
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>.
	 * The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 *
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0), processor.readRegister(Processor.regA0),
					processor.readRegister(Processor.regA1), processor.readRegister(Processor.regA2),
					processor.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			break;

		default:
			Lib.debug(dbgProcess, "Unexpected exception: " + Processor.exceptionNames[cause]);
			Lib.assertNotReached("Unexpected exception");
		}
	}

	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;	//	页表
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;	//	页数

	/** The number of pages in the program's stack. */
	protected final int stackPages = 8;	//	堆栈页数

	private int initialPC, initialSP;
	private int argc, argv;

	private static final int pageSize = Processor.pageSize;	//	页大小,整个系统中是固定的
	private static final char dbgProcess = 'a';
	
	private OpenFile openfile[]=new OpenFile[16];	//	打开文件表
	private int pid=0;	//	用户进程号
	public int status = 0;	//	进程状态；
	public boolean normalExit = false;//	退出状态
	public LinkedList<UserProcess> childProcess = new LinkedList<UserProcess>();	//	创建的子进程链表

	public UserProcess parentProcess = null;	//	当前进程的父进程
	private static int numOfProcess = 0;	//	进程计数器
	private static int numOfRunningProcess = 0;	//	运行进程计数器
	
	private Lock joinLock = new Lock();	//	join方法的锁
	private Condition joinCondition = new Condition(joinLock);	//	对应的条件变量
	
	private KThread thread;	//	内核线程
	
	
	
}
