package nachos.threads;

import nachos.machine.*;

/**
 * A KThread is a thread that can be used to execute Nachos kernel code. Nachos
 * allows multiple threads to run concurrently.
 * 
 * KThread是用来执行nachos内核代码的线程，nachos允许多线程同时运行。
 *
 * To create a new thread of execution, first declare a class that implements
 * the <tt>Runnable</tt> interface. That class then implements the <tt>run</tt>
 * method. An instance of the class can then be allocated, passed as an argument
 * when creating <tt>KThread</tt>, and forked. For example, a thread that
 * computes pi could be written as follows:
 * 
 * 为了创建一个新的线程，首先应声明一个实现Runnable接口的类。这个类实现Runnable接口的run方法。
 * 这个类的实例被作为参数分配和传递给创建的KThread，并且fork这个线程。
 * run方法里写的就是我们期望这个KThread执行的功能。
 *
 * <p>
 * <blockquote>
 * 
 * <pre>
 * class PiRun implements Runnable {
 *     public void run() {
 *         // compute pi
 *         ...
 *     }
 * }
 * </pre>
 * 
 * </blockquote>
 * <p>
 * The following code would then create a thread and start it running:
 *
 * <p>
 * <blockquote>
 * 
 * <pre>
 * PiRun p = new PiRun();
 * new KThread(p).fork();
 * </pre>
 * 
 * </blockquote>
 */
public class KThread {
	/**
	 * Get the current thread.
	 * 获得当前的K线程
	 *
	 * @return the current thread.
	 */
	public static KThread currentThread() {
		Lib.assertTrue(currentThread != null);
		return currentThread;
	}

	/**
	 * Allocate a new <tt>KThread</tt>. If this is the first <tt>KThread</tt>,
	 * create an idle thread as well.
	 * 
	 * 分配一个新的KThread。如果是第一个KThread，同时要创建一个空闲线程。
	 */
	public KThread() {
		if (currentThread != null) {
			//如果存在当前K线程，那么给它new一个TCB出来。后来的代码。
			tcb = new TCB();
		} else {
			
			/*
			 * 这是创建第一个KThread的代码。
			 */
			
			readyQueue = ThreadedKernel.scheduler.newThreadQueue(false);
			
			waitQueueForJoin = ThreadedKernel.scheduler.newThreadQueue(true);
			
			//在就绪队列请求获得当前KThread。acquire中要确保当前已经关闭中断，并且等待队列为空。
			readyQueue.acquire(this);

			//把当前KThread赋给currentThread
			currentThread = this;
			
			//由于当前的KThread一定有一个TCB支撑着，所以就把这个KThread的tcb设置为当前的TCB
			//因为这个是第一个KThread，所以顺序是java线程->TCB->KThread。所以这时候已经存在TCB了。
			tcb = TCB.currentTCB();	
			name = "main";// 每一个KThread都有一个名字。这个说明是主线程（第一个KThread
			restoreState();

			createIdleThread();//第二个线程
		}
	}

	/**
	 * Allocate a new KThread.
	 *
	 * @param target
	 *            the object whose <tt>run</tt> method is called.
	 */
	public KThread(Runnable target) {
		this(); // 执行不带参数的构造方法。
		this.target = target;//空闲线程将要执行的代码
	}

	/**
	 * Set the target of this thread.
	 *
	 * @param target
	 *            the object whose <tt>run</tt> method is called.
	 * @return this thread.
	 */
	public KThread setTarget(Runnable target) {
		Lib.assertTrue(status == statusNew);

		this.target = target;
		return this;
	}

	/**
	 * Set the name of this thread. This name is used for debugging purposes
	 * only.
	 *
	 * @param name
	 *            the name to give to this thread.
	 * @return this thread.
	 */
	public KThread setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get the name of this thread. This name is used for debugging purposes
	 * only.
	 *
	 * @return the name given to this thread.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the full name of this thread. This includes its name along with its
	 * numerical ID. This name is used for debugging purposes only.
	 *
	 * @return the full name given to this thread.
	 */
	public String toString() {
		return (name + " (#" + id + ")");
	}

	/**
	 * Deterministically and consistently compare this thread to another thread.
	 */
	public int compareTo(Object o) {
		KThread thread = (KThread) o;

		if (id < thread.id)
			return -1;
		else if (id > thread.id)
			return 1;
		else
			return 0;
	}

	/**
	 * Causes this thread to begin execution. The result is that two threads are
	 * running concurrently: the current thread (which returns from the call to
	 * the <tt>fork</tt> method) and the other thread (which executes its
	 * target's <tt>run</tt> method).
	 */
	public void fork() {	//	创建java线程
		Lib.assertTrue(status == statusNew);
		Lib.assertTrue(target != null);

		Lib.debug(dbgThread, "Forking thread: " + toString() + " Runnable: " + target);

		boolean intStatus = Machine.interrupt().disable();

		tcb.start(new Runnable() {	//tcb负责线程的一系列操作
			public void run() {
				runThread();
			}
		});

		ready();

		Machine.interrupt().restore(intStatus);
	}

	private void runThread() {
		begin();
		target.run();
		finish();
	}

	private void begin() {
		Lib.debug(dbgThread, "Beginning thread: " + toString());

		Lib.assertTrue(this == currentThread);

		restoreState();

		Machine.interrupt().enable();
	}

	/**
	 * Finish the current thread and schedule it to be destroyed when it is safe
	 * to do so. This method is automatically called when a thread's
	 * <tt>run</tt> method returns, but it may also be called directly.
	 *
	 * The current thread cannot be immediately destroyed because its stack and
	 * other execution state are still in use. Instead, this thread will be
	 * destroyed automatically by the next thread to run, when it is safe to
	 * delete this thread.
	 */
	public static void finish() {
		Lib.debug(dbgThread, "Finishing thread: " + currentThread.toString());

		Machine.interrupt().disable();

		Machine.autoGrader().finishingCurrentThread();

		Lib.assertTrue(toBeDestroyed == null);
		toBeDestroyed = currentThread;

		currentThread.status = statusFinished;
		
		KThread joinedKThread;
		
		joinedKThread = waitQueueForJoin.nextThread();
		if(joinedKThread!=null) {
			numOfJoin--;
			joinedKThread.ready();
		}

		sleep();
	}

	/**
	 * Relinquish the CPU if any other thread is ready to run. If so, put the
	 * current thread on the ready queue, so that it will eventually be
	 * rescheuled.
	 * 
	 * 如果另一个线程就绪， 那么让出CPU。如果让出，那么把当前这个线程加入到就绪队列，从而使它可以被重新调度
	 *
	 * <p>
	 * Returns immediately if no other thread is ready to run. Otherwise returns
	 * when the current thread is chosen to run again by
	 * <tt>readyQueue.nextThread()</tt>.
	 * 
	 * 如果没有其他线程准备run，那么立即返回当前线程
	 * 否则，在当前线程被就绪队列又一次选中运行时返回。
	 *
	 * <p>
	 * Interrupts are disabled, so that the current thread can atomically add
	 * itself to the ready queue and switch to the next thread. On return,
	 * restores interrupts to the previous state, in case <tt>yield()</tt> was
	 * called with interrupts disabled.
	 */
	public static void yield() {
		Lib.debug(dbgThread, "Yielding thread: " + currentThread.toString());

		Lib.assertTrue(currentThread.status == statusRunning);

		boolean intStatus = Machine.interrupt().disable();	//	先关中断

		currentThread.ready();	//当前线程进入就绪队列

		runNextThread();

		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Relinquish the CPU, because the current thread has either finished or it
	 * is blocked. This thread must be the current thread.
	 * 
	 * 必须出让CPU，因为调用sleep方法时说明当前线程要么完成了，要么被阻塞了
	 * 这个出让CPU的线程必须是当前线程。
	 *
	 * <p>
	 * If the current thread is blocked (on a synchronization primitive, i.e. a
	 * <tt>Semaphore</tt>, <tt>Lock</tt>, or <tt>Condition</tt>), eventually
	 * some thread will wake this thread up, putting it back on the ready queue
	 * so that it can be rescheduled. Otherwise, <tt>finish()</tt> should have
	 * scheduled this thread to be destroyed by the next thread to run.
	 * 
	 * 如果当前线程被阻塞（由于信号量，锁或条件变量），那么其他的某个线程必须要在某个时间将这个线程唤醒，
	 * 并把它放回就绪队列从而使之可以被重新调度。
	 * 否则，finish方法应该来调度当前线程，使它在下一个线程运行前被销毁。
	 */
	public static void sleep() {
		Lib.debug(dbgThread, "Sleeping thread: " + currentThread.toString());

		Lib.assertTrue(Machine.interrupt().disabled());

		if (currentThread.status != statusFinished)
			currentThread.status = statusBlocked;

		runNextThread();
	}

	/**
	 * Moves this thread to the ready state and adds this to the scheduler's
	 * ready queue.
	 */
	public void ready() {
		Lib.debug(dbgThread, "Ready thread: " + toString());

		Lib.assertTrue(Machine.interrupt().disabled());
		Lib.assertTrue(status != statusReady);

		status = statusReady;
		if (this != idleThread)
//			System.out.println("has wait for access");
			readyQueue.waitForAccess(this);

		Machine.autoGrader().readyThread(this);
	}

	/**
	 * Waits for this thread to finish. If this thread is already finished,
	 * return immediately. This method must only be called once; the second call
	 * is not guaranteed to return. This thread must not be the current thread.
	 */
	public void join() {
		Lib.debug(dbgThread, "Joining to thread: " + toString());

		Lib.assertTrue(this != currentThread);	//	确保不是当前线程自己调用了join

		boolean intStatus = Machine.interrupt().disable();	//	要切换线程了，系统关中断
		if(numOfJoin!=0&&this.status!=statusFinished) {
			System.out.println("too much join");
			return;
		}
		
		if(this.status!=statusFinished) {
			numOfJoin++;
			
			//大写加粗重要！
			waitQueueForJoin.acquire(this);//!!！
			waitQueueForJoin.waitForAccess(currentThread);	//	当前线程加入到join线程的等待队列中
//			System.out.println("join begin");
			KThread.currentThread().sleep();	//	当前线程休眠。
		}	
		Machine.interrupt().restore(intStatus);

	}

	/**
	 * Create the idle thread. Whenever there are no threads ready to be run,
	 * and <tt>runNextThread()</tt> is called, it will run the idle thread. The
	 * idle thread must never block, and it will only be allowed to run when all
	 * other threads are blocked.
	 *
	 * <p>
	 * Note that <tt>ready()</tt> never adds the idle thread to the ready set.
	 */
	private static void createIdleThread() {
		//确保空闲线程只有一个。
		Lib.assertTrue(idleThread == null);

		idleThread = new KThread(new Runnable() {//第二个线程进入KThread的构造函数
			public void run() {
				while (true)
					yield();//死循环。一直休眠。
			}
		});	//为空闲线程创建了KTH和TCB对象。
		idleThread.setName("idle");

		Machine.autoGrader().setIdleThread(idleThread);

		idleThread.fork();
	}

	/**
	 * Determine the next thread to run, then dispatch the CPU to the thread
	 * using <tt>run()</tt>.
	 */
	private static void runNextThread() {
		KThread nextThread = readyQueue.nextThread();//首先在就绪队列中找到下一个线程
		if (nextThread == null)
			nextThread = idleThread;//如果下一个线程为空，那么将下一个线程置为空闲线程

		nextThread.run();
	}

	/**
	 * Dispatch the CPU to this thread. Save the state of the current thread,
	 * switch to the new thread by calling <tt>TCB.contextSwitch()</tt>, and
	 * load the state of the new thread. The new thread becomes the current
	 * thread.
	 *
	 * <p>
	 * If the new thread and the old thread are the same, this method must still
	 * call <tt>saveState()</tt>, <tt>contextSwitch()</tt>, and
	 * <tt>restoreState()</tt>.
	 *
	 * <p>
	 * The state of the previously running thread must already have been changed
	 * from running to blocked or ready (depending on whether the thread is
	 * sleeping or yielding).
	 *
	 * @param finishing
	 *            <tt>true</tt> if the current thread is finished, and should be
	 *            destroyed by the new thread.
	 */
	private void run() {
		Lib.assertTrue(Machine.interrupt().disabled());

		Machine.yield();

		currentThread.saveState();	//	当前线程保存状态

		Lib.debug(dbgThread, "Switching from: " + currentThread.toString() + " to: " + toString());

		currentThread = this;

		tcb.contextSwitch();

		currentThread.restoreState();	//	当前线程恢复状态
	}

	/**
	 * Prepare this thread to be run. Set <tt>status</tt> to
	 * <tt>statusRunning</tt> and check <tt>toBeDestroyed</tt>.
	 */
	protected void restoreState() {
		Lib.debug(dbgThread, "Running thread: " + currentThread.toString());

		/*
		 * 确保中断关闭，确保当前线程和记录的currentThread线程相同，确保当前tcb和记录的currentTCB相同。
		 */
		Lib.assertTrue(Machine.interrupt().disabled());
		Lib.assertTrue(this == currentThread);
		Lib.assertTrue(tcb == TCB.currentTCB());

		Machine.autoGrader().runningThread(this);

		//把状态设置为running
		status = statusRunning;

		//如果一个线程被唤醒执行之前有一个线程将要被撤销了，那么就把那个线程给撤销。因为它本身不能撤销自己
		//也就是说，某一个线程要被撤销时，它先加入toBeDestroyed队列，因为它不能直接撤销自己。
		//下一个线程要开始的时候，检查以下这个撤销队列。把这些要被撤销的给撤销掉。
		if (toBeDestroyed != null) {
			toBeDestroyed.tcb.destroy();
			toBeDestroyed.tcb = null;
			toBeDestroyed = null;
		}
	}

	/**
	 * Prepare this thread to give up the processor. Kernel threads do not need
	 * to do anything here.
	 */
	protected void saveState() {
		Lib.assertTrue(Machine.interrupt().disabled());
		Lib.assertTrue(this == currentThread);
	}

	private static class PingTest implements Runnable {
		PingTest(int which) {
			this.which = which;
		}

		public void run() {
			for (int i = 0; i < 5; i++) {
				System.out.println("*** thread " + which + " looped " + i + " times");
				currentThread.yield();
			}
		}

		private int which;
	}
	
	private static class PingTest2 implements Runnable {
		PingTest2(int which) {
			this.which = which;
		}

		public void run() {
			for (int i = 0; i < 30; i++) {
				System.out.println("*** thread " + which + " looped " + i + " times");
				currentThread.yield();
			}
		}

		private int which;
	}

	/**
	 * Tests whether this module is working.
	 * 
	 * new一个新的K线程，让它开始pin，同时原线程也开始pin，这两个线程是同时存在的。
	 */
	public static void selfTest() {
		Lib.debug(dbgThread, "Enter KThread.selfTest");

		new KThread(new PingTest(1)).setName("forked thread").fork();
		new PingTest(0).run();
		System.out.println();
		System.out.println();
	}
	
	public static void testJoin() {
		Lib.debug(dbgThread, "Enter KThread.testJoin");
		
		System.out.println("******* test join() *******");

		KThread a = new KThread(new PingTest(4));
		a.fork();
		a.join();

		new PingTest(2).run();

		
		System.out.println();
		System.out.println();
	}
	
	public static void PriorityTest(){
		
		System.out.println("******* test PriorityScheduler *******");

	    boolean status = Machine.interrupt().disable();//关中断，setPriority()函数中要求关中断
	   
	    KThread a = new KThread(new PingTest(1)).setName("thread1");
	    new PriorityScheduler().setPriority(a,2);
	    System.out.println("priority of thread1: "+new PriorityScheduler().getThreadState(a).priority);
	    
	    KThread b = new KThread(new PingTest(2)).setName("thread2");
	    new PriorityScheduler().setPriority(b,2);
	    System.out.println("priority of thread2: "+new PriorityScheduler().getThreadState(b).priority);
	   
	    KThread c = new KThread(new Runnable(){
	        public void run(){
	            for (int i=0; i<5; i++) {
	                if(i==2) {
//	                	System.out.println("a.join");
	                	a.join();
	                }
	                System.out.println("*** thread 3 looped " + i + " times");
	                KThread.yield();
	            }
	        }
	    }).setName("thread3");
	    new PriorityScheduler().setPriority(c,7);
	    System.out.println("priority of thread3: "+new PriorityScheduler().getThreadState(c).priority);
	    
	    a.fork();
	    b.fork();
	    c.fork();
	    KThread.yield();
//	    a.join();
//	    c.join();
//	    b.join();
	    Machine.interrupt().restore(status);
	    System.out.println();
	    System.out.println();
	}
	
	public static void LotteryTest(){
		
		System.out.println("******* test LotteryScheduler *******");

	    boolean status = Machine.interrupt().disable();//关中断，setPriority()函数中要求关中断
	    
	    KThread a = new KThread(new PingTest2(1)).setName("thread1");
	    new LotteryScheduler().setPriority(a,1);
	    System.out.println("tiks of thread1: "+new LotteryScheduler().getThreadState(a).priority);
	    
	    KThread b = new KThread(new PingTest2(2)).setName("thread2");
	    new LotteryScheduler().setPriority(b,3);
	    System.out.println("tiks of thread2: "+new LotteryScheduler().getThreadState(b).priority);
	   
	    KThread c = new KThread(new Runnable(){
	        public void run(){
	            for (int i=0; i<40; i++) {
//	                if(i==1) 
//	                    a.join();
	                System.out.println("*** thread 3 looped "+ i + " times");
	                KThread.yield();
	            }
	        }
	    }).setName("thread3");
	    new LotteryScheduler().setPriority(c,5);
	    System.out.println("tiks of thread3: "+new LotteryScheduler().getThreadState(c).priority);
	    
	    a.fork();
	    b.fork();
	    c.fork();
	    KThread.yield();
//	    a.join();
//	    c.join();
//	    b.join();
	    Machine.interrupt().restore(status);
	    System.out.println();
	    System.out.println();
	}


	private static final char dbgThread = 't';

	/**
	 * Additional state used by schedulers.
	 *
	 * @see nachos.threads.PriorityScheduler.ThreadState
	 */
	public Object schedulingState = null;

	private static final int statusNew = 0;
	private static final int statusReady = 1;
	private static final int statusRunning = 2;
	private static final int statusBlocked = 3;
	private static final int statusFinished = 4;

	/**
	 * The status of this thread. A thread can either be new (not yet forked),
	 * ready (on the ready queue but not running), running, or blocked (not on
	 * the ready queue and not running).
	 */
	private int status = statusNew;
	private String name = "(unnamed thread)";
	private Runnable target;
	private TCB tcb;

	/**
	 * Unique identifer for this thread. Used to deterministically compare
	 * threads.
	 */
	private int id = numCreated++;
	/** Number of times the KThread constructor was called. */
	private static int numCreated = 0; // 已经创建的线程数量

	private static ThreadQueue waitQueueForJoin = null;
	private static int numOfJoin=0;
	
	private static ThreadQueue readyQueue = null;
	private static KThread currentThread = null;
	private static KThread toBeDestroyed = null;// 对将要被销毁的线程做标记，其他线程遇到做标记的线程时
	private static KThread idleThread = null;// 空闲线程，每当将CPU分配给他们时，立即放弃CPU，然后将CPU返还给调度程序。然后继续执行调度程序。如果系统空闲，那么就是空闲线程和调度程序始终在互相切换。
}
