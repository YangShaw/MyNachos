package nachos.threads;

import nachos.ag.BoatGrader;

public class Boat {
	static BoatGrader bg;
	static int childO;	//	左岸孩子
	static int childM;	//	右岸孩子
	static int adultO;	//	左岸大人
	static int adultM;	//	右岸大人
	static boolean boatO;	//	船在左岸
//	static boolean boatM;	//	船在右岸
	static boolean end;		//	任务完成
	static boolean adultRound;	//	轮到大人
	static boolean pilotRound;	//	轮到驾驶员（两个小孩一组）
	static Lock lock;
	static Condition childConditionO;
	static Condition childConditionM;
	static Condition adultConditionO;
	static Semaphore boatSem;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

//		System.out.println("\n ***Testing Boats with only 2 children***");
//		begin(0, 2, b);

//		 System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
//		 begin(1, 2, b);
		 
		 System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		 begin(3, 3, b);
		 
		 KThread.yield();
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;
		childO = children;
		childM = 0;
		adultO = adults; 
		adultM = 0;
		boatO = true;
		end = false; 
		adultRound = false;
		pilotRound = true;
		lock = new Lock();
		childConditionO = new Condition(lock);
		childConditionM = new Condition(lock);
		adultConditionO = new Condition(lock);
		boatSem = new Semaphore(0);
		
		System.out.println("begin");
		for(int i=0;i<adults;i++) {
			new KThread(new Runnable() {
				public void run() {
					AdultItinerary();
				}
			}).setName("adult"+i).fork();
		}
			
		for(int i=0;i<children;i++) {
			KThread cur = new KThread(new Runnable() {
				public void run() {
					ChildItinerary();
				}
			});
			cur.setName("child"+i);
			cur.fork();
//			if(i==children-1) {
//				cur.join();
//			}
		}
		
		boatSem.P();
		System.out.println("success for boat!");

		// Instantiate global variables here

		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.

//		Runnable r = new Runnable() {
//			public void run() {
//				SampleItinerary();
//			}
//		};
//		KThread t = new KThread(r);
//		t.setName("Sample Boat Thread");
//		t.fork();

	}

	static void AdultItinerary() {
		bg.initializeAdult(); // Required for autograder interface. Must be the first thing called.
		// DO NOT PUT ANYTHING ABOVE THIS LINE.

		/*
		 * This is where you should put your solutions. Make calls to the BoatGrader to
		 * show that it is synchronized. For example: bg.AdultRowToMolokai(); indicates
		 * that an adult has rowed the boat across to Molokai
		 */
		lock.acquire();
		if(!adultRound||!boatO) {
			//如果没有轮到大人走 或者船不在O 那么大人休眠
			adultConditionO.sleep();
		} 
		bg.AdultRowToMolokai();
		adultM++;
		adultO--;
		boatO = false;	//	船到M了
		childConditionM.wake();	//	唤醒M岛的child.把船开回去
		adultRound = false;	//	先默认不允许大人走，后面还要判断小孩子数量
		lock.release();//释放锁
		
	}

	static void ChildItinerary() {
		bg.initializeChild(); // Required for autograder interface. Must be the first thing called.
		// DO NOT PUT ANYTHING ABOVE THIS LINE.
		lock.acquire();
		while(!end) {
			if(boatO) {
				//	船在O岛的情况
				if(adultRound) {
					//	如果轮到大人走
					adultConditionO.wake();
					childConditionO.sleep();
				} else if(pilotRound) {
					//	第一个小孩
					bg.ChildRowToMolokai();
					childO--;
					childM++;
					pilotRound = false;	//	下一次轮到第二个小孩
					
					childConditionO.wake();	//	唤醒O岛中的一个孩子线程
					childConditionM.sleep();	//	当前线程加入M岛等待队列
				} else {
					//	第二个小孩
					bg.ChildRideToMolokai();
					boatO = false;	//	凑足了两个小孩才开船，所以此刻船才到M岛
					childO--;
					childM++;
					pilotRound = true;	//	下一次又轮到第一个小孩
					if(adultO==0&&childO==0) {
						end = true;	//成功！
//						System.out.println("success for boat!");
						boatSem.V();
						childConditionO.sleep();
					} 
//					if(end) {
//						
//					}
					if(adultO!=0&&childO==0) {
						adultRound = true;	//	O岛的孩子走光了，终于轮到大人走了
						System.out.println("adult true");
					}
					childConditionM.wake();	//	唤醒M岛的等待线程，因为这时候需要M岛的小孩开船回去了
					childConditionM.sleep();	//	当前线程M岛等待队列
				}
			} else {
				//	船在M岛，只有一种情况，找一个倒霉的小孩把船开回去
				bg.ChildRowToOahu();//就自己一个人，肯定是驾船的
				boatO = true;	//	船开回去了
				childO++;
				childM--;
				if(adultRound){	//	前面提到的O岛孩子走光了的情况，事实上现在O岛又有了一个孩子
					adultConditionO.wake();
				} else {
					childConditionO.wake();
				}
				childConditionO.sleep();
			}
		}
		lock.release();
	}

	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}

}
