package nachos.threads;

import nachos.machine.*;

import java.util.LinkedList;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Iterator;

/**
 * A scheduler that chooses threads using a lottery.
 *
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 *
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 *
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking
 * the maximum).
 */
public class LotteryScheduler extends Scheduler {
    /**
     * Allocate a new lottery scheduler.
     */
    public LotteryScheduler() {
    }
    
    /**
     * Allocate a new lottery thread queue.
     *
     * @param	transferPriority	<tt>true</tt> if this queue should
     *					transfer tickets from waiting threads
     *					to the owning thread.
     * @return	a new lottery thread queue.
     */
    public ThreadQueue newThreadQueue(boolean transferPriority) {
	// implement me
    	return new LotteryQueue(transferPriority);
    }
    
    public int getPriority(KThread thread) {//得到线程的优先级  
    	Lib.assertTrue(Machine.interrupt().disabled());            
    	return getThreadState(thread).getPriority();     
    }
    
    public void setPriority(KThread thread, int priority) {//设置线程优先级  
    	Lib.assertTrue(Machine.interrupt().disabled()); 
        Lib.assertTrue(priority >= priorityMinimum && priority <= priorityMaximum);    
        
        getThreadState(thread).setPriority(priority);     
    }
    
    public boolean increasePriority() {//增加运行线程的优先级  
    	boolean intStatus = Machine.interrupt().disable();            
    	KThread thread = KThread.currentThread();   
    	int priority = getPriority(thread);  
    	if (priority == priorityMaximum)      
    		return false;   
    	
    	setPriority(thread, priority+1);   
    	
    	Machine.interrupt().restore(intStatus);  
    	return true;     
    	}
    
    public boolean decreasePriority() {//降低运行线程的优先级  
    	boolean intStatus = Machine.interrupt().disable();            
    	KThread thread = KThread.currentThread();  
    	
    	int priority = getPriority(thread);  
    	if (priority == priorityMinimum)      
    		return false;   
    	
    	setPriority(thread, priority-1);   
    	
    	Machine.interrupt().restore(intStatus);  
    	return true;     
    	}
    
    
    public static final int priorityDefault = 1;    //新线程默认优先级
    public static final int priorityMinimum = 0;    //线程最低优先级是0 
    public static final int priorityMaximum = 7;    //线程最高优先级是7 
    
    protected ThreadState getThreadState(KThread thread) {//得到线程的优先级状态，如果线程优先级未创建则创建为默认优先级  
    	if (thread.schedulingState == null)      
    		thread.schedulingState = new ThreadState(thread);   
    	
    	return (ThreadState) thread.schedulingState;     
    	}

    
    protected class LotteryQueue extends ThreadQueue {//优先级队列类，继承自线程队列        
    	LotteryQueue(boolean transferPriority) {//自动调用父类无参数构造方法，创建一个线程队列         
    		
    		this.transferPriority = transferPriority;  
    		}   
    	
		public void waitForAccess(KThread thread) {//传入等待队列的线程      
			Lib.assertTrue(Machine.interrupt().disabled());   
			
			getThreadState(thread).waitForAccess(this);        
			} 
		
		 public void acquire(KThread thread) {      
			 Lib.assertTrue(Machine.interrupt().disabled());            
			 if(!thread.getName().equals("main"))      
			 {       
				 getThreadState(thread).acquire(this);      
			 }  
			 }

		@Override
		public KThread nextThread() {
			Lib.assertTrue(Machine.interrupt().disabled());
			 if(waitQueue.isEmpty())       
				 return null;
			 int tiksCount=0; 
			 for(int i=0;i<waitQueue.size();i++)//计算队列中所有彩票的总数      
			 {  
				 ThreadState thread=waitQueue.get(i);         
				 tiksCount=tiksCount+thread.getEffectivePriority();	//	有效优先级，所有的加起来          
			 }
			 int numOfWin=Lib.random(tiksCount+1);//产生中奖的数目
			 
	//		 System.out.println("winner is :"+numOfWin);
			 
			 int nowtickets=0;      
			 KThread winThread=null;      
			 ThreadState thread=null;      
			 for(int i=0;i<waitQueue.size();i++)//顺序遍历等待队列，得到获胜的线程      
			 {  
				 thread=waitQueue.get(i);         
				 nowtickets=nowtickets+thread.getEffectivePriority();       
				 if(nowtickets>=numOfWin)        
				 {
					 winThread=thread.thread;        
					 break;        
				 }                
			 }
			 
			 if(winThread!=null)       
				 waitQueue.remove(thread);
			 return winThread; 
		}
		
		protected ThreadState pickNextThread() { 
			return null;
		}
		@Override
		public void print() {
			// TODO 自动生成的方法存根
			Lib.assertTrue(Machine.interrupt().disabled());
			
		}
	
		public boolean transferPriority;  
		public LinkedList<ThreadState> waitQueue = new LinkedList<ThreadState>();  
		public ThreadState linkedthread=null;  
	}
    
    protected class ThreadState{
    	
		public ThreadState(KThread thread){
			this.thread = thread;
			setPriority(priorityDefault);
			acquired = new LotteryQueue(true);
	    }
		
		public int getPriority(){
			return priority;
		}
		
		public int getEffectivePriority(){//得到有效优先级
			effectivepriority = priority;
			
			for(int i = 0; i < acquired.waitQueue.size(); i++){
				//把等待线程持有的彩票的总数给这个线程
				effectivepriority = effectivepriority+acquired.waitQueue.get(i).getEffectivePriority();
			}
			return effectivepriority;
			
		}
	
		public void setPriority(int priority){//优先级传递
			if(this.priority == priority)
				return;
			this.priority = priority;
		}
	    
		
		public void waitForAccess(LotteryQueue waitQueue){
			//将此线程状态存入传入的等待队列
			waitQueue.waitQueue.add(this);
			if(waitQueue.linkedthread!=null&&waitQueue.linkedthread!=this)   
			{      
				waitQueue.linkedthread.acquired.waitForAccess(this.thread);   
			}
		}
	    
	    public void acquire(LotteryQueue waitQueue) 
	    {//相当于一个线程持有的队列锁        
	    	waitQueue.linkedthread=this;  
	    }
	    	
	   
		protected KThread thread;//这个对象关联的线程    
		protected int priority;//关联线程的优先级   
		protected int effectivepriority;//有效优先级    
		protected LotteryQueue acquired; 

}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
