package nachos.threads;

import nachos.machine.*;
import nachos.threads.PriorityScheduler.PriorityQueue;

import java.util.TreeSet;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * A scheduler that chooses threads based on their priorities.
 *
 * <p>
 * A priority scheduler associates a priority with each thread. The next thread
 * to be dequeued is always a thread with priority no less than any other
 * waiting thread's priority. Like a round-robin scheduler, the thread that is
 * dequeued is, among all the threads of the same (highest) priority, the thread
 * that has been waiting longest.
 *
 * <p>
 * Essentially, a priority scheduler gives access in a round-robin fassion to
 * all the highest-priority threads, and ignores all other threads. This has the
 * potential to starve a thread if there's always a thread waiting with higher
 * priority.
 *
 * <p>
 * A priority scheduler must partially solve the priority inversion problem; in
 * particular, priority must be donated through locks, and through joins.
 */
public class PriorityScheduler extends Scheduler {
	/**
	 * Allocate a new priority scheduler.
	 */
	public PriorityScheduler() {
	}

	/**
	 * Allocate a new priority thread queue.
	 *
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer priority from
	 *            waiting threads to the owning thread.
	 * @return a new priority thread queue.
	 * 
	 * 
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new PriorityQueue(transferPriority);
	}

	public int getPriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadState(thread).getPriority();
	}

	public int getEffectivePriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadState(thread).getEffectivePriority();
	}

	public void setPriority(KThread thread, int priority) {
		Lib.assertTrue(Machine.interrupt().disabled());

		Lib.assertTrue(priority >= priorityMinimum && priority <= priorityMaximum);

		getThreadState(thread).setPriority(priority);
	}

	public boolean increasePriority() {	//	增加优先级
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMaximum)
			return false;

		setPriority(thread, priority + 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	public boolean decreasePriority() {	//	降低优先级
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMinimum)
			return false;

		setPriority(thread, priority - 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	/**
	 * The default priority for a new thread. Do not change this value.
	 */
	public static final int priorityDefault = 1;
	/**
	 * The minimum priority that a thread can have. Do not change this value.
	 */
	public static final int priorityMinimum = 0;
	/**
	 * The maximum priority that a thread can have. Do not change this value.
	 */
	public static final int priorityMaximum = 7;

	/**
	 * Return the scheduling state of the specified thread.
	 *
	 * @param thread
	 *            the thread whose scheduling state to return.
	 * @return the scheduling state of the specified thread.
	 * 
	 * 获得线程的状态
	 */
	protected ThreadState getThreadState(KThread thread) {
		if (thread.schedulingState == null)
			thread.schedulingState = new ThreadState(thread);

		return (ThreadState) thread.schedulingState;
	}

	/**
	 * A <tt>ThreadQueue</tt> that sorts threads by priority.
	 * 一个靠优先级排序的线程队列
	 */
	protected class PriorityQueue extends ThreadQueue {
		PriorityQueue(boolean transferPriority) {
			this.transferPriority = transferPriority;
			
		}

		public void waitForAccess(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			getThreadState(thread).waitForAccess(this);
		}

		public void acquire(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			getThreadState(thread).acquire(this);
		}

		public KThread nextThread() {
			Lib.assertTrue(Machine.interrupt().disabled());
			// implement me

			ThreadState next = pickNextThread();	//	下一个线程
			if(next==null) {	//	没有下一个线程了
				return null;
			} else {
				
				KThread thread = next.thread;
				getThreadState(thread).acquire(this);
				
				return thread;
			}
		}

		/**
		 * Return the next thread that <tt>nextThread()</tt> would return,
		 * without modifying the state of this queue.
		 *
		 * @return the next thread that <tt>nextThread()</tt> would return.
		 */
		protected ThreadState pickNextThread() {
			// implement me
			Iterator<KThread> i = waitList.iterator();
			KThread next;
			if(i.hasNext()) {
				next=i.next();
				KThread cur = null;
				while(i.hasNext()) {	//	比较队列中的各个线程的有效优先级
					cur = i.next();
					int a = getThreadState(next).getEffectivePriority();
					int b = getThreadState(cur).getEffectivePriority();
					if(a<b) {	//	等待时间长的先出队
						next = cur;
					}
				}
				return getThreadState(next);
			} else {
				return null;
			}
		}

		public void print() {
			Lib.assertTrue(Machine.interrupt().disabled());
			// implement me (if you want)
		}

		/**
		 * <tt>true</tt> if this queue should transfer priority from waiting
		 * threads to the owning thread.
		 */
		public boolean transferPriority;
		protected KThread lockHolder = null;	//	队列头，持有锁的线程
		protected LinkedList<KThread> waitList = new LinkedList<KThread>();
		
	}

	/**
	 * The scheduling state of a thread. This should include the thread's
	 * priority, its effective priority, any objects it owns, and the queue it's
	 * waiting for, if any.
	 *
	 * @see nachos.threads.KThread#schedulingState
	 * 
	 * 表示每个线程的调度状态。包括了线程的优先级，有效优先级，它需要等待资源的线程队列
	 */
	protected class ThreadState {
		
		protected int effectivePriority = -2;	//	初始化有效优先级
		protected final int invalidPriority = -1;
		protected HashSet<PriorityScheduler.PriorityQueue> acquired = new HashSet<PriorityScheduler.PriorityQueue>();
		//等待该线程的所有优先队列的无序无重复集合
		
		/**
		 * Allocate a new <tt>ThreadState</tt> object and associate it with the
		 * specified thread.
		 *
		 * @param thread
		 *            the thread this state belongs to.
		 */
		public ThreadState(KThread thread) {
			this.thread = thread;

			setPriority(priorityDefault);
		}

		/**
		 * Return the priority of the associated thread.
		 *
		 * @return the priority of the associated thread.
		 */
		public int getPriority() {
			return priority;
		}

		/**
		 * Return the effective priority of the associated thread.
		 * 获取有效优先级
		 *
		 * @return the effective priority of the associated thread.
		 */
		public int getEffectivePriority() {

			Lib.assertTrue(Machine.interrupt().disabled());
//			if(effectivePriority==invalidPriority&&!acquired.isEmpty()){
			if(effectivePriority==invalidPriority){
				//如果有效优先级无效并且等待队列的集合不为空
				effectivePriority=priority;	//	先赋给自己的优先级；
//				if(!acquired.isEmpty()) {
//					System.out.println("not empyt");
//				}
				for(Iterator<PriorityQueue> i = acquired.iterator();i.hasNext();){
					//比较acquired中所有等待队列中的所有线程的优先级
					
					for(Iterator<KThread> j=i.next().waitList.iterator();j.hasNext();) {
						//对每个队列比较其中所有线程
						ThreadState ts = getThreadState((KThread)j.next());	//获取当前队列中的下一个线程
//						System.out.println(ts.priority);
						if(ts.priority>effectivePriority) {
							//和当前的有效优先级进行比较，找出一个最大的来
							effectivePriority=ts.priority;
						}
					}
				}
				if(effectivePriority>priority) {
					System.out.println("priority donating happened, changing from "+priority+" to "+effectivePriority);
				}
				return effectivePriority;
			} else {
				if(effectivePriority==-2) {
					//该优先级线程队列不需要进行优先级donate
					return priority;	//	那么只需要返回线程本身的优先级就可以了
				}
				else {
					return effectivePriority;	//	说明之前计算过有效优先级了，那么仍然可以拿过来用
				}
			}
			
		}

		/**
		 * Set the priority of the associated thread to the specified value.
		 *
		 * @param priority
		 *            the new priority.
		 */
		public void setPriority(int priority) {
			if (this.priority == priority)
				return;

			this.priority = priority;

			// implement me
		}

		/**
		 * Called when <tt>waitForAccess(thread)</tt> (where <tt>thread</tt> is
		 * the associated thread) is invoked on the specified priority queue.
		 * The associated thread is therefore waiting for access to the resource
		 * guarded by <tt>waitQueue</tt>. This method is only called if the
		 * associated thread cannot immediately obtain access.
		 *
		 * @param waitQueue
		 *            the queue that the associated thread is now waiting on.
		 *
		 * @see nachos.threads.ThreadQueue#waitForAccess
		 */
		public void waitForAccess(PriorityQueue waitQueue) {
			// implement me
			waitQueue.waitList.add(this.thread);		//	当前线程加入等待队列
		}

		/**
		 * Called when the associated thread has acquired access to whatever is
		 * guarded by <tt>waitQueue</tt>. This can occur either as a result of
		 * <tt>acquire(thread)</tt> being invoked on <tt>waitQueue</tt> (where
		 * <tt>thread</tt> is the associated thread), or as a result of
		 * <tt>nextThread()</tt> being invoked on <tt>waitQueue</tt>.
		 * 
		 * 把当前线程从等待队列中取出，并置为队列头。
		 *
		 * @see nachos.threads.ThreadQueue#acquire
		 * @see nachos.threads.ThreadQueue#nextThread
		 */
		public void acquire(PriorityQueue waitQueue) {
//			System.out.println("ps acquire");
			// implement me
			waitQueue.waitList.remove(this.thread);	//	如果这个队列中存在该线程，那么删除
			
			
			waitQueue.lockHolder=this.thread;	//	
			if(waitQueue.transferPriority) {	//	如果存在优先级倒置，用invalid这个量来标识一下。
				this.effectivePriority = invalidPriority;
				acquired.add(waitQueue);	//	将等待该线程的队列加入该线程的等待队列的集合中（更新后）
			}
		}

		/** The thread with which this object is associated. */
		protected KThread thread;
		/** The priority of the associated thread. */
		protected int priority;
	}
}
