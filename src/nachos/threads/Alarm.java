package nachos.threads;

import java.util.LinkedList;
import java.util.TreeMap;

import nachos.machine.*;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {
	private static char dbgAlarm = 'a';

	/**
	 * Allocate a new Alarm. Set the machine's timer interrupt handler to this
	 * alarm's callback.
	 * 
	 * 把系统的时钟中断处理器赋给这个定时器
	 *
	 * <p>
	 * <b>Note</b>: Nachos will not function correctly with more than one alarm.
	 */
	public Alarm() {
		Machine.timer().setInterruptHandler(new Runnable() {
			public void run() {
				timerInterrupt();	
			}
		});
	}

	/**
	 * The timer interrupt handler. This is called by the machine's timer
	 * periodically (approximately every 500 clock ticks). Causes the current
	 * thread to yield, forcing a context switch if there is another thread that
	 * should be run.
	 */
	public void timerInterrupt() {
		boolean intStatus = Machine.interrupt().disable();
		long currentTime = Machine.timer().getTime();
		int size = waitQueue.size();
		if(size==0) {
			;
		} else {
			for(int i=0;i<waitQueue.size();i++){
				if(currentTime>=waitQueue.get(i).getWakeTime()){	//	允许被唤醒的条件
					waitQueue.get(i).getThread().ready();
					System.out.println(waitQueue.get(i).getWakeTime()+" has been chosen.");
					waitQueue.remove(i);
//					i=0;
					
				}
				currentTime=Machine.timer().getTime();	//	每次刷新一下时间
			}
		}

		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Put the current thread to sleep for at least <i>x</i> ticks, waking it up
	 * in the timer interrupt handler. The thread must be woken up (placed in
	 * the scheduler ready set) during the first timer interrupt where
	 *
	 * <p>
	 * <blockquote> (current time) >= (WaitUntil called time)+(x) </blockquote>
	 *
	 * @param x
	 *            the minimum number of clock ticks to wait.
	 *
	 * @see nachos.machine.Timer#getTime()
	 */
	public void waitUntil(long x) {	//	加入等待队列
		// for now, cheat just to get something working (busy waiting is bad)
		boolean intStatus = Machine.interrupt().disable();
		long wakeTime = Machine.timer().getTime() + x;
		WaitingKThread waitingKThread = new WaitingKThread(KThread.currentThread(), wakeTime);
		waitQueue.add(waitingKThread);
		System.out.println("waitUntil: inserted and wakeTime should be "+wakeTime);
		//有序插入链表
//		int size = waitQueue.size();
//		if(size == 0){
//			waitQueue.add(waitingKThread);
//			System.out.println("insert in first 0 and wakeTime is "+wakeTime);
//		}
//		for(int i=0;i<size;i++){
//			if(wakeTime<waitQueue.get(i).getWakeTime()){
//				waitQueue.add(i, waitingKThread);
//				System.out.println("insert in i "+i+" and wakeTime is "+wakeTime);
//			} else
//			if(i==size-1&&wakeTime>=waitQueue.get(i).getWakeTime()){
//				waitQueue.add(i+1,waitingKThread);
//				System.out.println("insert in i+1 "+(i+1)+" and wakeTime is "+wakeTime);
//			}
//		}
		
		KThread.sleep();
		Machine.interrupt().restore(intStatus);
	}
	
	private class WaitingKThread{
		KThread thread;
		private long wakeTime;
		public WaitingKThread(KThread thread, long wakeTime) {
			this.thread = thread;
			this.wakeTime = wakeTime;
		}
		public long getWakeTime(){
			return wakeTime;
		}
		public KThread getThread(){
			return thread;
		}
	}
	
	public static void selfTest(){
		Lib.debug(dbgAlarm , "Alarm Self Test");	
		System.out.println("******* test Alarm *******");
		// Test that alarm wakes up thread after proper amount of time
		Alarm alarm = new Alarm();
		
		KThread thread = new KThread(new Runnable() {
			public void run() {
				final long ticks = 1050;
				System.out.println("thread1 should wait for at least "+ticks+" ticks");
				long sleepTime = Machine.timer().getTime();
				alarm.waitUntil(ticks);
				long wakeTime = Machine.timer().getTime();				
				if(wakeTime-sleepTime>=ticks){
					System.out.println("thread1 start at "+sleepTime+" and end at "+wakeTime);
					System.out.println("thread1 should sleep "+ticks+
							", and it acturally slept "+(wakeTime-sleepTime));
					alarmSem.V();
				} else {
					System.out.println("fail");
				}
			}
		});
		thread.fork();
		
		KThread thread2 = new KThread(new Runnable() {
			public void run() {
				final long ticks = 300;
				System.out.println("thread2 should wait for at least "+ticks+" ticks");
				long sleepTime = Machine.timer().getTime();
				alarm.waitUntil(ticks);
				long wakeTime = Machine.timer().getTime();			
				if(wakeTime-sleepTime>=ticks){
					System.out.println("thread2 start at "+sleepTime+" and end at "+wakeTime);
					System.out.println("thread2 should sleep "+ticks+
							", and it acturally slept "+(wakeTime-sleepTime));
				} else {
					System.out.println("fail");
				}
			}
		});
		thread2.fork();
		alarmSem.P();	//	防止等待时间太长
		
		System.out.println();
		System.out.println();
	}
	//存储所有调用了waitUntil方法的休眠线程
	public static LinkedList<WaitingKThread> waitQueue = new LinkedList<WaitingKThread>(); 
	private static Semaphore alarmSem = new Semaphore(0);
}
