package nachos.threads;

import nachos.machine.*;

/**
 * A <tt>Semaphore</tt> is a synchronization primitive with an unsigned value. A
 * semaphore has only two operations:
 * 
 * 信号量是一个无符号的同步原语，对信号量只有两种操作。
 * P：等待，直到信号量的值大于0.然后-1；
 * V：增加信号量的值，然后唤醒一个在P方法中等待的线程。
 *
 * <ul>
 * <li><tt>P()</tt>: waits until the semaphore's value is greater than zero,
 * then decrements it.
 * <li><tt>V()</tt>: increments the semaphore's value, and wakes up one thread
 * waiting in <tt>P()</tt> if possible.
 * </ul>
 *
 * <p>
 * Note that this API does not allow a thread to read the value of the semaphore
 * directly. Even if you did read the value, the only thing you would know is
 * what the value used to be. You don't know what the value is now, because by
 * the time you get the value, a context switch might have occurred, and some
 * other thread might have called <tt>P()</tt> or <tt>V()</tt>, so the true
 * value might now be different.
 *
 *这个API不允许直接读信号量的值。
 */
public class Semaphore {
	/**
	 * Allocate a new semaphore.
	 *
	 * @param initialValue
	 *            the initial value of this semaphore.
	 *            给信号量赋初始值，也就是初始的个数（可利用的资源数）。
	 */
	public Semaphore(int initialValue) {
		value = initialValue;
	}

	/**
	 * Atomically wait for this semaphore to become non-zero and decrement it.
	 * 
	 * 原子地等待信号量变成一个非零数，然后减少它。
	 */
	public void P() {
		boolean intStatus = Machine.interrupt().disable();	//先关中断，确保原子性。

		if (value == 0) {
//			System.out.println(KThread.currentThread().getName()+":sleep");
			waitQueue.waitForAccess(KThread.currentThread());	
			KThread.sleep();	//	当前线程休眠，并且唤醒就绪队列中的下一个线程
		} else {
			value--;
		}

		Machine.interrupt().restore(intStatus);	//	恢复中断
	}

	/**
	 * Atomically increment this semaphore and wake up at most one other thread
	 * sleeping on this semaphore.
	 * 
	 * 原子地增加信号量并且唤醒最多一个其他的正在休眠中的线程。
	 */
	public void V() {
		boolean intStatus = Machine.interrupt().disable();	//先关中断

		KThread thread = waitQueue.nextThread();	//	获取等待队列中的下一个线程
		if (thread != null) {
//			System.out.println("has ready");
//			System.out.println(thread.getName()+":wake");
			thread.ready();	//	如果存在下一个线程，那么唤醒它（也意味着刚刚增加的这个信号量被使用了）
		} else {
			value++;	//	如果不存在下一个线程，那么信号量的值加一（也意味着可利用的资源增加了一个）
		}

		Machine.interrupt().restore(intStatus);	//	恢复中断	
	}

	private static class PingTest implements Runnable {
		PingTest(Semaphore ping, Semaphore pong) {
			this.ping = ping;
			this.pong = pong;
		}

		public void run() {
			for (int i = 0; i < 10; i++) {
				ping.P();
				pong.V();
			}
		}

		private Semaphore ping;
		private Semaphore pong;
	}

	/**
	 * Test if this module is working.
	 * 
	 * 测试信号量是否有效。
	 */
	public static void selfTest() {
		Semaphore ping = new Semaphore(0);
		Semaphore pong = new Semaphore(0);

		//创建一个新线程进行测试。
		new KThread(new PingTest(ping, pong)).setName("ping").fork();

		for (int i = 0; i < 10; i++) {
			ping.V();
			pong.P();
		}
	}

	private int value;
	private ThreadQueue waitQueue = ThreadedKernel.scheduler.newThreadQueue(false);
}
