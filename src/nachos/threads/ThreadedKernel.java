package nachos.threads;

import nachos.machine.*;
import nachos.userprog.UserKernel;

/**
 * A multi-threaded OS kernel.
 */
public class ThreadedKernel extends Kernel {
	/**
	 * Allocate a new multi-threaded kernel.
	 */
	public ThreadedKernel() {
		super();
	}

	/**
	 * Initialize this kernel. Creates a scheduler, the first thread, and an
	 * alarm, and enables interrupts. Creates a file system if necessary.
	 * 
	 * ThreadedKernel继承了抽象类Kernel， 实现它的方法，从AG的start方法中进入这里。
	 * 初始化中，创建一个调度规则，创建第一个线程，一个计时器，并开启中断。
	 * 如果必要的话，创建一个文件系统。
	 */
	public void initialize(String[] args) {
		// set scheduler
		//	从配置文件中读取这一项的内容，proj1用的是轮转法调度（RR）。
		String schedulerName = Config.getString("ThreadedKernel.scheduler");
		scheduler = (Scheduler) Lib.constructObject(schedulerName);

		// set fileSystem
		String fileSystemName = Config.getString("ThreadedKernel.fileSystem");
		if (fileSystemName != null)
			fileSystem = (FileSystem) Lib.constructObject(fileSystemName);
		else if (Machine.stubFileSystem() != null)
			fileSystem = Machine.stubFileSystem();
		else
			fileSystem = null;

		// start threading
		new KThread(null); 

		alarm = new Alarm();

		//打开中断
		Machine.interrupt().enable();
	}

	/**
	 * Test this kernel. Test the <tt>KThread</tt>, <tt>Semaphore</tt>,
	 * <tt>SynchList</tt>, and <tt>ElevatorBank</tt> classes. Note that the
	 * autograder never calls this method, so it is safe to put additional tests
	 * here.
	 * 
	 * 测试这个内核，包括测试KThread，信号量，同步列表和……
	 * AG不会调用这个方法，所以在这里可以添加额外的测试。
	 */
	public void selfTest() { // 来自AG的selfTest。分为三个部分。
		KThread.testJoin();	//	自己编写的测试方法，测试join能否执行。
		Semaphore.selfTest(); // 测试信号量是否能实现线程之间的同步
		SynchList.selfTest();	//	测试communicator
		Alarm.selfTest();	//	测试计时器
		Communicator.selfTest();
		KThread.PriorityTest();
		Boat.selfTest();
//		KThread.LotteryTest();

		if (Machine.bank() != null) {
			ElevatorBank.selfTest();
		}
	}

	/**
	 * A threaded kernel does not run user programs, so this method does
	 * nothing.
	 */
	public void run() {
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		Machine.halt();
	}

	/** Globally accessible reference to the scheduler. */
	public static Scheduler scheduler = null;
	/** Globally accessible reference to the alarm. */
	public static Alarm alarm = null;
	/** Globally accessible reference to the file system. */
	public static FileSystem fileSystem = null;

	// dummy variables to make javac smarter
	private static RoundRobinScheduler dummy1 = null;
	private static PriorityScheduler dummy2 = null;
	private static LotteryScheduler dummy3 = null;
	private static Condition2 dummy4 = null;
	private static Communicator dummy5 = null;
	private static Rider dummy6 = null;
	private static ElevatorController dummy7 = null;
}
