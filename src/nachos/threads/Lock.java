package nachos.threads;

import nachos.machine.*;

/**
 * A <tt>Lock</tt> is a synchronization primitive that has two states,
 * <i>busy</i> and <i>free</i>. There are only two operations allowed on a lock:
 *
 *锁只有两个状态：busy和free。对锁有两种操作：
 *
 * <ul>
 * <li><tt>acquire()</tt>: atomically wait until the lock is <i>free</i> and
 * then set it to <i>busy</i>.
 * <li><tt>release()</tt>: set the lock to be <i>free</i>, waking up one waiting
 * thread if possible.
 * </ul>
 *
 *请求：原子地等待这个锁，直到锁变为free态，然后获得这个锁，并将它设置为busy态。
 *释放：把这个锁设置为free态，唤醒一个等待中的线程
 * <p>
 * Also, only the thread that acquired a lock may release it. As with
 * semaphores, the API does not allow you to read the lock state (because the
 * value could change immediately after you read it).
 * 
 * 显然，只有持有锁的线程才能释放它。
 */
public class Lock {
	/**
	 * Allocate a new lock. The lock will initially be <i>free</i>.
	 */
	public Lock() {
	}

	/**
	 * Atomically acquire this lock. The current thread must not already hold
	 * this lock.
	 */
	public void acquire() {
		Lib.assertTrue(!isHeldByCurrentThread());

		boolean intStatus = Machine.interrupt().disable();
		KThread thread = KThread.currentThread();

		/*
		 * lockHolder表示持有这个锁的KThread对象。如果该对象不为空，说明这个锁已经被持有，
		 * 即锁处于busy状态。那么把当前线程加入到等待队列。
		 * 
		 * 如果为空，说明这个锁处于free状态，那么执行等待队列中的这个线程，并把lockHolder设置为该线程
		 */
		if (lockHolder != null) {
			waitQueue.waitForAccess(thread);
			KThread.sleep();
		} else {
			waitQueue.acquire(thread);
			lockHolder = thread;
		}

		Lib.assertTrue(lockHolder == thread);

		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Atomically release this lock, allowing other threads to acquire it.
	 */
	public void release() {
		Lib.assertTrue(isHeldByCurrentThread());

		boolean intStatus = Machine.interrupt().disable();

		/*
		 * 如果等待队列不为空，那么把锁交给它，并使这个线程进入就绪状态
		 */
		if ((lockHolder = waitQueue.nextThread()) != null)
			lockHolder.ready();

		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Test if the current thread holds this lock.
	 *
	 * @return true if the current thread holds this lock.
	 */
	public boolean isHeldByCurrentThread() {
		return (lockHolder == KThread.currentThread());
	}

	private KThread lockHolder = null;
	private ThreadQueue waitQueue = ThreadedKernel.scheduler.newThreadQueue(true);
}
