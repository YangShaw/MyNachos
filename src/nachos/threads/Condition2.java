package nachos.threads;

import java.util.LinkedList;

import nachos.machine.*;

/**
 * An implementation of condition variables that disables interrupt()s for
 * synchronization.
 *
 * <p>
 * You must implement this.
 *
 * @see nachos.threads.Condition
 */
public class Condition2 {
	/**
	 * Allocate a new condition variable.
	 *
	 * @param conditionLock
	 *            the lock associated with this condition variable. The current
	 *            thread must hold this lock whenever it uses <tt>sleep()</tt>,
	 *            <tt>wake()</tt>, or <tt>wakeAll()</tt>.
	 */
	public Condition2(Lock conditionLock) {
		this.conditionLock = conditionLock;
		waitQueue = new LinkedList<KThread>();
	}

	/**
	 * Atomically release the associated lock and go to sleep on this condition
	 * variable until another thread wakes it using <tt>wake()</tt>. The current
	 * thread must hold the associated lock. The thread will automatically
	 * reacquire the lock before <tt>sleep()</tt> returns.
	 */
	public void sleep() {
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		
		boolean intStatus = Machine.interrupt().disable();	//	关中断
		conditionLock.release();
		
		/*
		 * 值得注意的是，这里利用互斥锁的等待队列来控制条件变量的等待队列。互斥锁release，当前线程
		 * 加入了互斥锁的wait队列，，然后我们让当前线程也加入这个条件的wait队列。这样就实现了
		 * 条件变量和它相关联的锁的同步。
		 */
		waitQueue.add(KThread.currentThread());	//	释放锁后，当前线程加入等待队列
		KThread.currentThread().sleep();	//	当前线程休眠。
		/*
		 * 在sleep之后，当前线程已经被挂起了，进入了别的线程，在别的线程中，会唤醒这个线程，
		 * 所以在返回到这个线程之后，接下来的语句让这个线程重新获得锁。也就是说，这个线程
		 * 进入等待队列之后会在其他线程中被从等待队列里提取出来。
		 */
		
		
		conditionLock.acquire();
		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Wake up at most one thread sleeping on this condition variable. The
	 * current thread must hold the associated lock.
	 */
	public void wake() {
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		
		boolean intStatus = Machine.interrupt().disable();
		KThread nextThread;
		if(!waitQueue.isEmpty()){
			nextThread = waitQueue.removeFirst();
			nextThread.ready();
		}
		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Wake up all threads sleeping on this condition variable. The current
	 * thread must hold the associated lock.
	 */
	public void wakeAll() {
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		while(waitQueue!=null){
			wake();
		}
	}
	

	private Lock conditionLock;
	private LinkedList<KThread> waitQueue;
}
