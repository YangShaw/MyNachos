package nachos.threads;

import java.util.LinkedList;
import java.util.Queue;

import nachos.machine.*;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		lock = new Lock();
		queue = new LinkedList<Integer>();
		condition1 = new Condition2(lock);
		condition2 = new Condition2(lock);
		word = -1;
		speakerNum=0;
		listenerNum=0;
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 *
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 *
	 * @param word
	 *            the integer to transfer.
	 */
	
	/*
	 * 
	 */
	public void speak(int word) {
		boolean intStatus = Machine.interrupt().disabled();
		lock.acquire();	//	有锁才可以进行操作
		speakerNum++;
		queue.offer(word);
		if(listenerNum==0){	//	没有人听，那么说话者数目加一
			System.out.println("waiting for listening");
			condition1.sleep();	//	条件一休眠
//			listenerNum--;
		} else{
			condition2.wake();
			listenerNum--;
		}
		
		lock.release();
		Machine.interrupt().restore(intStatus);
		return;
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 *
	 * @return the integer transferred.
	 */
	public int listen() {
		boolean intStatus = Machine.interrupt().disabled();
		lock.acquire();
		listenerNum++;
		if(speakerNum==0){	//	如果没有人说话，就休眠，从休眠中恢复的时候，说明有说话者了，那么说话者--
			System.out.println("waiting for speaker");
			condition2.sleep();
//			speakerNum--;
		} else {
			condition1.wake();	//	如果有人说话，就唤醒它，说话的人--
			speakerNum--;
		}
		lock.release();
		Machine.interrupt().restore(intStatus);
		
		word = queue.poll();
		return word;
	}
	
	private static class Speaker implements Runnable {
	    private Communicator c;
	    private String name;
	    Speaker(Communicator c, String name) {
	        this.c = c;
	        this.name = name;
	    }
	    public void run() {
	        for (int i = 0; i < 5; ++i) {
	            System.out.println("speaker "+name+" is speaking " + i);
	            c.speak(i);
	            //System.out.println("speaker spoken");
	            KThread.yield();
	        }
	    }
	}
	public static void selfTest() {
		System.out.println("******* test Communicator *******");

		System.out.println("listener is ready.");
	    Communicator c = new Communicator();
	    new KThread(new Speaker(c,"1")).setName("Speaker").fork();	//	创建一个新线程来说话
	    System.out.println("speaker1 is ready.");
	    
	    new KThread(new Speaker(c,"2")).setName("Speaker").fork();	//	创建一个新线程来说话
	    System.out.println("speaker2 is ready.");
	    for (int i = 0; i < 10; ++i) {	//	当前线程来收听
//	        System.out.println("listener listening " + i);
	        int x = c.listen();
	        System.out.println("count "+i+":listener has listened " + x);
	        KThread.yield();
	    }
	    
	    System.out.println();
	    System.out.println();
	}
	

	private Condition2 condition1, condition2;	//	表示说话者，收听者
	private int word;
	private Lock lock;
	private Queue<Integer> queue;	//	说话者想说的话的队列
	private int speakerNum, listenerNum;	//	当前等待的说话者和收听者个数
}
